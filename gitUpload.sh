#!/bin/bash
echo " > Starting to upload to GitLab..."
echo " > Adding.."
git add .
git status
echo " > Applying changes.."
git commit -a -m "Auto-commit Update"
echo " > Pushing to Server, login to gitlab:"
git push
echo " > Update Complete!"
