#!/bin/bash
echo " > Starting Aircrack-ng Listener.."
sleep 3
sudo airmon-ng check kill
sudo airmon-ng start wlan0
sudo airodump-ng wlan0mon
sleep 5
exit
echo " > Choose Target by BSSID. "
read -p "Enter BSSID: " bssid
read -p "Enter Channel: " ch
read -p "Enter save file name: " file
gnome-terminal -x sh -c "./airc2.sh; bash"
sudo airodump-ng --bssid $bssid -c $ch --write $file wlan0mon
echo " > Done."

